import { Routes } from '@angular/router';

import { CadastroPecaComponent } from './cadastro-peca.component';

export const CadastroPecaRoutes: Routes = [{
  path: '',
  component: CadastroPecaComponent,
  data: {
    heading: 'Cadastro de Peça',
    removeFooter: true
  }
}];
