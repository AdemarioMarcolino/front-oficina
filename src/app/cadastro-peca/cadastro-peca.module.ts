import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SidebarModule } from 'ng-sidebar';

import { CadastroPecaComponent } from './cadastro-peca.component';
import { CadastroPecaRoutes } from './cadastro-peca.routing';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
// import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  imports: [
    CommonModule, 
    RouterModule.forChild(CadastroPecaRoutes), 
    SidebarModule,
    NgxDatatableModule
  ],
  declarations: [CadastroPecaComponent]
})

export class CadastroPecaModule {}
