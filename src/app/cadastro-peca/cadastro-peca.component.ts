import { Component, OnInit, } from '@angular/core';
import { CadastroPecaService } from './../servicos/cadastro-peca.service'

@Component({
  selector: 'app-cadastro-peca',
  templateUrl: './cadastro-peca.component.html',
  styleUrls: ['./cadastro-peca.component.scss'],
  providers: []
})

export class CadastroPecaComponent implements OnInit {
  novaPeca = false;
  titlePeca = 'Lista de Peças';

  rows:any[];
  columns = [
    { prop: 'name' },
    { name: 'Gender' },
    { name: 'Age' }
  ];
  constructor(
    private cadastroPecaService:CadastroPecaService
  ){}

  ngOnInit(): void {
    this.cadastroPecaService.getPecas().subscribe(
      data=>{
        this.rows = data.rows;
        console.log(this.rows);
        
      }
    )
  }
  
}
