import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class CadastroPecaService {

  constructor(
    private http: HttpClient,
  ) {}

  getPecas() { return this.http.get<any>('http://localhost:9000/api/peca/listar')};

}
