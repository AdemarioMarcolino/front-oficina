import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '/',
    name: 'HOME',
    type: 'link',
    icon: 'basic-accelerator'
  },
  {
    state: 'cadastro-peca',
    name: 'Cadastro de Peça',
    type: 'link',
    icon: 'basic-paperplane'
  },
  {
    state: 'calendar',
    name: 'CALENDAR',
    type: 'link',
    icon: 'basic-calendar'
  },
  
  {
    state: 'widgets',
    name: 'WIDGETS',
    type: 'link',
    icon: 'software-scale-reduce'
  },
  {
    state: 'social',
    name: 'SOCIAL',
    type: 'link',
    icon: 'basic-elaboration-message-happy'
  },
  {
    state: 'docs',
    name: 'DOCS',
    type: 'link',
    icon: 'basic-sheet-txt'
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
